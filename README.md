# Codes and data for reviewing the results presented in the submitted paper: "An overview of variance-based importance measures in the linear regression context: comparative analyses and numerical tests".
# Authors: L. Clouvel, V. Chabridon, M. Il Idrissi, B. Iooss, F. Robin
##########################################################################################

## Repository structure

There are two files: 
- 'toydata_UC1-10.R' is the main file. It describes the 11 UseCases. 
- 'VIM.R' defines the function 'computeIM' formatting the input data of a selected usecase and launching the different methods requested by the user.

The user can choose to compute the results of various use cases by changing the vector "UseCases" and for specific methods by changing the vector "Methods". 

!!!! Warning: the use cases 8, 9, 10 and 11 can be particularly time-consuming during the computation of the LMG/PMVD methods.

The results are printed in the output.txt file. All the plots will be save in a repository called 'plots' which has been created before the launching.

## Software versions

The following versions of the different software/packages used are the following:

### Software

- R : 4.1.1
- RStudio : 1.1.463

### Packages

- sensitivity     1.29.0 - for the functions src, pcc, lmg, pmvd and correlRatio
- rwa              0.0.3 - for the functions rwa
- car              3.1-1 - for the function vif
- mvtnorm          1.1-3 - for the function rmvnorm
- boot            1.3-28 - for the functions cv.glm and boot
- mlbench          2.1-3 - for BostonHousing2 dataset
- caret           6.0-93 - for cars dataset
- bannerCommenter  1.0.0 - for the function banner
- ggplot2          3.3.5 - for the function ggplot
- GGally           2.1.2 - for the function ggpairs
